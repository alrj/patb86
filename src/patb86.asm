; kate: mode Intel x86 (NASM)
;************************************************************************
;*
;*                     PATB86
;*             Tiny Basic Interpreter
;*                   Version 3.1
;*              For Intel x86 systems
;*                   Amand Tihon
;*                 11 March, 2019
;*
;* This is a port of the Palo Alto Tiny Basic to the x86 architecture.
;* It is based on Li-Chen Wang's 8080 Tiny Basic.
;*
;* This port tries to keep as much as possible the same logic and the
;* same organisation in the code as in the original work.
;*
;************************************************************************
;*
;* Many of the comments, and especially the blocks that are present in
;* front of each section, are from the original listing, only adapted 
;* when needed.
;*
;************************************************************************

;========================================================================
%define         GOOD_RND    1

section .text

; Build it as a .COM executable
org 100h

;========================================================================
; Initialize
PATB86_Init:

    ; Initialize RND seed
    push ds
    mov ax, 40h
    mov ds, ax                  ; Access BIOS data zone
    mov cx, [06ch]              ; Get tick counter (low word)
    mov dx, [06eh]              ; (high word)
    pop ds
    mov [ranseed], cx           ; Store random seed (low word)
    mov [ranseed+2], dx         ; (high word)
    

purge:
    mov word [txtunf], txtbgn   ; Purge text area

tell:
    xor ah, ah
    mov si, msg
    call prtstg                 ; Tell user.


;========================================================================
; Direct command / Text collecter
;
; TBI prints out "OK(CR)", and then it prompts ">" and reads a line.
; If the line starts with a non-zero number, this number is the line
; number. The line number (in 16 bit binary) and the reste of the line
; (including CR) is stored in the memory. If a line with the same
; line number is already there, it is replaced by the new one. If the
; rest of the line consists of a CR only, it is not stored and any
; existing line with the same line number is deleted.
;
; After a line is inserted, replaced, or deleted, the program loops
; back and ask for another line. This loop will be terminated when it
; reads a line with zero or no line number; and control is transfered
; to 'direct'.
;
; Tiny Basic program save area starts at the memory location labeled
; 'text'. The content of the location labeled 'textunf' points to the
; end of the text. It is followed by @(0).
;
; The memory location 'currnt' points to the line number that is 
; currently being interpreted. While we are in this loop or while we
; are interpreting a direct command, (see next section), 'currnt'
; should point to a 0.
rstart:
    xor ax, ax
    mov sp, stack               ; Stack is cleared
    mov [currnt], ax            ; Current line # = 0
    mov [lopvar], ax            ; Loop params are cleared
    mov [stkgos], ax            ; Gosub return address is cleard
    
    mov si, ok
    call prtstg                 ; Print "Ok"
    
    
prompt_loop:
    mov al, '>'                 ; Prompt > and
    call getln                  ; read a line. di -> end of line
    
    mov si, buffer              ; si -> beginning of line
    call tstnum                 ; test if it is a number
    push ax
    call ignblnk
    pop ax                      ; ax = value of # or 0 if no # was found
    or cl, cl                   ; any digits ?
    jz direct                   ; if not, it's a direct command/statement
    
    ; At this point, we have a numbered line to store in the text area.
    ; ax contains the line number.
    ; si points to the first char of the statement to store
    ; di still points to the end of the line (after the CR)
    dec si
    dec si                      ; move si back two bytes
    mov [si], ax                ; and prepend value of line number there
    mov bx, si                  ; bx -> begin, including line #
    mov dx, di                  ; dx -> line end
    call fndln                  ; find this line in save 
    push si                     ; area, si-> save area
    jnz .insert_line            ; NZ: line was not found, insert it
    
    push si                     ; Z: found the line, delete it
    call fndnxt                 ; si -> start of line to keep
    pop di                      ; di-> line to be deleted
    mov cx, [txtunf]            ; to the end of the save area
    sub cx, si                  ; how many bytes to move ?
    rep movsb                   ; move them
    mov [txtunf], di            ; update value

.insert_line:                   ; get ready to insert
    pop ax                      ; ax-> start of line to move down
    sub dx, bx                  ; first check if lenght of line is 3 (# and CR)
    cmp dx, 3                   ; Then, do not insert, only delete and
    je rstart                   ; clear the stack.
    
    mov cx, [txtunf]
    mov si, cx
    sub cx, ax                  ; cx=amount of bytes to move down
    
    mov di, si
    dec si                      ; si-> current last byte of text save area
    add di, dx                  ; di-> new txtunf
    cmp di, txtend              ; check if there is enough space
    jae qsorry                  ; sorry, no room for it
    mov [txtunf], di            ; ok, update it
    dec di                      ; di-> new last byte of text save area
    std                         ; Move backward !
    rep movsb                   ; move all the bytes
    cld                         ; restore forward direction
    
    mov cx, dx                  ; dx was still the length of the buffer line
    mov di, ax                  ; ax was still the place to insert it
    mov si, bx                  ; bx was still pointing to the text to insert
    rep movsb                   ; finally store the new line
    
    jmp prompt_loop


;========================================================================
; Direct and exec
;
; This section of the code tests a string against a table. When a
; match is found, control is transfered to the section of code
; according to the table.
;
; At 'exec', si should point to the string and bx should point to the table.
; At 'direct', si should point to the string and bx will be set up to point to
; 'table1', which is the table of all direct and statements commands.
;
; A '.' in the string will terminate the test and the partial match will be 
; considered as a match, e.g. 'P.', 'PR.', 'PRI.', 'PRIN.', or 'PRINT' will all
; match 'PRINT'.
;
; The table consists of any number of items, each item is a null-terminated 
; string, and a pointer to a jump address.
;
; End of table is an empty string with a jump address. If the string does not
; match any of the other items, it will match this null item as default.

direct:
    mov bx, table1              ; Direct command execution

exec:
    call ignblnk

.ex_loop:
    push si                     ; save pointer
    
.ex_testchar:
    lodsb                       ; if found '.' in string before any mismatch
    cmp al, '.'                 ; we declare a match.
    je .ex_partial_match

    cs mov ah, [bx]
    inc bx
    or al, 20h                  ; lowercase al
    
    cmp al, ah                  ; Characters are still matching ?
    je .ex_testchar             ; then test next one
    
    or ah, ah                   ; else, see if we checked against the whole
    jz .ex_match                ; table item. Yes? then we have a match.
    
.ex_skip:                       ; else, match failed, bump to next table entry
    cs mov ah, [bx]             ;
    inc bx                      ; Find the end of this non-matching string
    or ah, ah                   ; 
    jnz .ex_skip                ; 
    inc bx
    inc bx                      ; Get past the jump address
    pop si                      ; Restore string pointer
    jmp .ex_loop                ; Test against next item.
    
.ex_partial_match:              ; Partial match.
    cs mov ah, [bx]             ; Find jump address which is after the null
    inc bx
    or ah, ah                   ; byte terminating the string
    jnz .ex_partial_match       ; more character to consume
    jmp .ex_end                 ; we're at the end
    
.ex_match:                      ; full match. 
    dec si                      ; si needs to go back one byte

.ex_end:
    pop ax                      ; Dummy pop to restore stack state
    cs jmp [bx]                 ; And we go to it


;=========================================================================
; What follows is the code to execute direct and statement commands.
; Control is transfered to these points via the command table lookup code
; of "direct" and "exec" in last section. After the command is executed,
; control is transfered to other setions as follow:
;
; For 'list', 'new', and 'stop': go back to 'rstart'.
; For 'run': go execute the first stored line if any; 
;            else go back to 'rstart'.
; For 'goto' and 'gosub': go execute the target line.
; For 'return' and 'next': go back to saved return line.
; For all others: if 'currnt' -> 0, go to 'rstart', else go execute next
;                 command. (This is done in 'finish').

; 'new(CR)' reset 'textunf'
new:
    call endchk
    jmp purge
    
    
; 'stop(CR)' goes back to 'rstart'
stop:
    call endchk
    jmp rstart


; 'run(CR)' finds the first stored line, store its address (in 'currnt') 
; and start execute it. Note that only those commands in 'table2' are 
; legal for a stored program.
; 
; There are three more entries in 'run':
; 'runnxl' finds next line, store its address in si and executes it.
; 'runtsl' stores the address of this line and executes it.
; 'runsml' continues the execution on same line
run:
    call endchk
    mov si, txtbgn              ; first saved line

runnxl:
    xor ax, ax                  ; 
    call fndlnp                 ; find whatever line # at [si] or after
    jc rstart                   ; C: passed txtunf, quit
    
runtsl:
    mov [currnt], si            ; store address of current line (-> the line #)
    inc si
    inc si                      ; bump pass the line #
    
runsml:
    call chkio                  ; Check for Control-X
    mov bx, table2              ; find command in table2
    jmp exec                    ; and execute it
    
    
; 'goto expr(CR)' evaluates the expression, find the target line and 
; jump to 'runtsl' to do it.
goto:
    call exp
    push si                     ; save for error routine
    call endchk                 ; must find a CR
    mov ax, bx                  ; the result of the call to expr is the line #
    call fndln                  ; find the target line
    jnz ahow                    ; No such line #
    pop ax                      ; dummy pop to clean up stack
    jmp runtsl                  ; go do it


;=========================================================================
; List has three forms:
; 'list(CR) lists all saved lines
; 'list N(CR) starts list at line N
; list N1, N2(CR) starts list at line N1 for N2 lines.
; You can stop the listing with Control-C.
list:
    call tstnum                 ; Test if there is a line #
    push ax
    call ignblnk
    xor cx, cx                  ; default value: 0
    cmp al, ','                 ; Do we have N2 as well ?
    jne .no_n2                  ; if we don't, 
    
    inc si                      ; if we do, go past the ',' 
    call tstnum                 ; and get that second number
    mov cx, ax                  ; overwrite the default value
    
.no_n2:
    call endchk                 ; Error if there's more on the line
    pop ax                      ; Get first line # back
    call fndln                  ; Find this line or next line
.list_loop:
    jc rstart                   ; C: passed textunf
    
    call prtln                  ; Print line #
    call prtstg                 ; Print line text
    dec cx                      ; Decrement the line counter
    jz rstart                   ; If N2 lines have been printed, we are done
    call chkio                  ; Interrupted ?
    xor ax, ax                  ; Don't need the line # anymore, we're further
    call fndlnp                 ; Find next line
    jmp .list_loop
    

; Print command is "PRINT ....;" or "PRINT ....(CR)"
; Where '....' is a list of expressions, formats, and/or strings.
; These items are separated by commas.
;
; A format is a hash sign followed by a number. It controls the
; number of spaces the value of an expression is going to be printed.
; It stays effective for the rest of the PRINT command, unles changed
; by another format. If no format is specified, 6 positions will be used.
;
; A string is quoted in a pair of single quotes or a pair of double quotes.
;
; Control characters and lower case letters can be included inside the 
; quotes. Another (better) way of generating control characters on
; the output is use the caret character followed by a letter. ^[ is ESC,
; ^H is Backspace, ^G is BELL, etc.
;
; A (CRLF) is generated after the entire list has been printed or if
; the list is a null list. However, if the list ended with a comma, no 
; (CRLF) is generated.
print:
    mov cl, 6                   ; cl = # of spaces
    call ignblnk
    cmp al, ";"                 ; if null list and ';'
    jne .pr1
    call crlf                   ; give CR-LF,
    inc si                      ; eat the character, and
    jmp runsml                  ; continue same line
.pr1:
    cmp al, 0dh                 ; if null list (CR)
    jne .pr6
    call crlf                   ; also give CR-LF,
    inc si                      ; eat the character, and
    jmp runnxl                  ; go to next line
.pr2:
    cmp al, '#'                 ; Else, is it format?
    jne .pr4
    inc si                      ; Eat the character
.pr3:
    call exp                    ; Yes, evaluate expression
    cmp bx, 63                  ; Maximum 63
    jae qhow                    ; Unsigned comparison matches negative values
    mov cl, bl                  ; set new format
    jmp .pr5                    ; Look for more to print
.pr4:
    call qtstg                  ; Or is it a string?
    jc .pr9                     ; If not, must be expr.
.pr5:
    call ignblnk
    cmp al, ','                 ; if ',' go find next
    jne .pr8
.pr6:
    cmp al, ','                 ; if comma
    jne .pr7
    ;mov al, ' '                 ; print a space
    ;call outch
    inc si                      ; eat the character
    call ignblnk
    jmp .pr6
.pr7:
    call fin                    ; Are we done ?
    jmp .pr2                    ; list continues
.pr8:
    call crlf                   ; list ends
    jmp finish
.pr9:
    push cx                     ; Save cl (format)
    call exp                    ; Evaluate the expression
    pop cx                      ; Restore cl
    xchg ax, bx                 ; put value in ax
    call prtnum                 ; Print the value
    xchg ax, bx                 ; get everyting back in place
    jmp .pr5


;=========================================================================
; 'gosub expr;' or 'gosub expr(CR)' is like the 'goto' command,
; except that the current text pointer, stack pointer, etc. are saved so
; that execution can be continued after the subroutine 'return'. In
; order that 'gosub' can be nested (and even recursive), the save area
; must be stacked. The stack pointer is saved in 'stkgos'. The old
; 'stkgos' is saved in the stack. If we are in the main routine,
; 'stkgos' is zero (this is done by the "main" section of the code),
; but we still save it as a flag for no further 'return's.
gosub:
    call _pusha                 ; Save the current "for" parameters
    call exp
    push si                     ; and text pointer
    mov ax, bx
    call fndln                  ; Find the target line
    jnz ahow                    ; Not there, say "How?"
    mov ax, [currnt]            ; save old
    push ax                     ; "currnt", old "stkgos"
    mov ax, [stkgos]
    push ax
    xor ax, ax                  ; And load new ones
    mov [lopvar], ax
    mov [stkgos], sp
    jmp runtsl                  ; Then run that line


; 'return(CR)' undos everything that 'gosub' did, and thus return the
; execution to the command after the most recent 'gosub'. If 'stkgos'
; is zero, it indicates that we never had a 'gosub' and is thus an
; error.
return:
    call endchk                 ; There must be a CR
    mov ax, [stkgos]            ; Old stack pointer
    or ax, ax                   ; 0 means not exist
    jz qwhat                    ; so, we say: "What?"
    mov sp, ax                  ; else, restore it
    pop ax
    mov [stkgos], ax            ; and the old "stkgos"
    pop ax
    mov [currnt], ax            ; and the old "currnt"
    pop si                      ; old text pointer
    call _popa                  ; old "for" parameters
    jmp finish


;=========================================================================
; 'for' has two forms: "for var=exp1 to exp2 step exp3" and "for
; var=exp1 to exp2". The second form means the same thing as the first
; form with exp3=1. (i.e., with a step of +1.) TBI will find the
; variable var. and set its value to the current value of exp1. It 
; also evaluates exp2 and exp3 and save all these together with the
; 'lopvar', 'lopinc', 'loplmt', 'lopln', and 'loppt'. If there is
; already somthing in the save area (this is indicated by a
; non-zero 'lopvar'), then the old save area is saved in the stack
; before the new overwrites it. TBI will then dig in the stack
; and find out if this same variable was used in another currently
; active "for" loop. If that is the case, then the old "for" loop is
; deactivated (purged from the stack).
for:
    call _pusha                 ; Save the old save area
    call setval                 ; Set the control var.
    mov [lopvar], di            ; di is its address, save that
    
    mov bx, table4              ; Use 'exec' to look
    jmp exec                    ; for the word "to"

for_to:
    call exp                    ; Evaluate the limit
    mov [loplmt], bx            ; store that
    
    mov bx, table5              ; Use 'exec' to look
    jmp exec                    ; for the word "step"

for_step:
    call exp                    ; found it, get step
    jmp fr4
fr3:
    mov bx, 1                   ; Not found, set to 1

fr4:
    mov [lopinc], bx            ; save that too
    mov ax, [currnt]            ; save current line #
    mov [lopln], ax
    mov [loppt], si             ; and text pointer

    mov bp, sp                  ; Here is the stack
    xor cx, cx                  ; Level counter
    jmp .fr6

.fr5:
    add bp, 10                  ; Each level is 10 deep

.fr6:
    add cx, 10                  ; One more level
    mov ax, [bp]                ; Get that old 'lopvar'
    or ax, ax
    jz .fr7                     ; 0 says no more in it
    cmp ax, [lopvar]            ; Same as this one ?
    jne .fr5                    ; No, check further
    
    mov ax, ds                  ; save segment register
    mov bx, ss
    mov ds, bx
    mov es, bx                  ; ds and es = ss
    
    add sp, 10                  ; Try to move sp
    
    mov si, bp
    dec si                      ; point to last byte of previous frame
    mov di, si
    add di, 10                  ; To be moved 10 bytes (5 words) further
    std                         ; Move backward
    rep movsb                   ; Purge 10 bytes
    cld                         ; Restore forward direction
    
    mov ds, ax
    mov es, ax                  ; restore segment registers
.fr7:
    mov si, [loppt]             ; Job done, restore si
    jmp finish                  ; and continue


; "next var" serves as the logical (not necessarily physical) end of
; the "for" loop. The control variable var. is checked with the
; 'lopvar'. If they are not the same, TBI digs in the stack to find
; the right one and purges all those that did not match. Either way,
; TBI then adds the 'step' to that variable and checks the result with
; the limit. If it is within the limit, control loops back to the
; command following the "for". If outside the limit, the save area is
; purged and execution continues.
next:
    call tstv                   ; Get address of var.
    jc qwhat                    ; No variable, say "What?"
    mov [varnxt], bx            ; Yes, save it

.nx1:
    push si                     ; Save text pointer
    mov ax, [lopvar]            ; Get var. in 'for'
    or ax, ax                   ; 0 says never had one
    jz awhat                    ; so we ask: "What?"
    cmp ax, bx                  ; else, check them
    je .nx2                     ; OK, they agree
    pop si                      ; No, let's see
    call _popa                  ; Purge current loop
    jmp .nx1                    ; and go check again

.nx2:                           ; Come here when agreed
    mov dx, [bx]                ; dx = value of var.
    mov ax, [lopinc]
    add dx, ax                  ; Add one step
    mov [bx], dx                ; Put it back
    jo .nx6                     ; Any overflow means we're out
    
    or ax, ax                   ; Are we going up to
    js .nx_downto               ; or down to the limit?
    cmp dx, [loplmt]            ; Compare with limit
    jg .nx6                     ; outside limit (going up)
    jmp .nx4
    
.nx_downto:
    cmp dx, [loplmt]            ; Compare with limit
    jl .nx6                     ; outside limit (going down)

.nx4:
    pop si                      ; dummy pop to cleanup stack
    mov ax, [lopln]             ; Within limit, go
    mov [currnt], ax            ; back to the saved
    mov si, [loppt]             ; 'currnt' and text pointer
    jmp finish

.nx6:
    pop si
    call _popa                  ; Purge this loop
    jmp finish


;=========================================================================
; 'rem' can be followed by anything and is ignored by TBI. TBI treats 
; it like an 'if' with a false condition.
rem:
    xor bx, bx
    jmp iff.if1                 ; This is like "IF 0"


; 'if' is followed by an expr. as a condition and one or more commands
; (including other 'if's) separated by semi-colons. Note that the
; word 'then' is not used. TBI evaluates the expr. If it is non-zero,
; the execution continues. If the expr. is zero, the commands that
; follows are ignored and execution continues at the next line.
iff:
    call exp
.if1:
    or bx, bx                   ; is the expr. = 0 ?
    jnz runsml                  ; no, continue
    xor ax, ax
    call fndskp                 ; yes, skip rest of line
    jnc runtsl                  ; and run the next line
    jmp rstart                  ; if no next, re-start
    

; 'input' command is like the 'print' command, and is followed by a
; list of items. If the item is a string in single or double quotes,
; or is a caret, it has the same effect as in 'print'. If an item
; is a variable, this variable name is printed out followed by a
; colon. Then TBI waits for an expr. to be typed in. The variable is
; then set to the value of this expr. If the variable is preceded by
; a string (again in single or double quotes), the string will be
; printed folloed by a colon. TBI then waits for input expr. and
; set the variable to the value of the expr.
;
; If the input expr. is invalid, TBI will print "What?", "How?" or
; "Sorry." and reprint the prompt and redo the input. The execution
; will not terminate unless you type CTRL-C. This is handled in
; 'inperr'.
inperr:
    mov sp, [stkinp]            ; Restore old sp
    pop ax
    mov [currnt], ax            ; and old 'currnt'
    pop si                      ; and old text pointer
    pop si                      ; redo input


input:
    push si                     ; save in case of error
    call ignblnk
    call qtstg                  ; Is next item a string?
    jc .ip8                     ; No
.ip2:
    call tstv                   ; Yes, but followed by a
    jc .ip5                     ; variable?  No.
    mov di, bx                  ; destination variable in di

.ip3:
    call .ip12
    mov si, buffer              ; Point to buffer
    call exp                    ; evaluate input
    call endchk
    pop di                      ; OK, get old di (variable address)
    mov [di], bx                ; save value in var
    pop ax
    mov [currnt], ax            ; get old 'currnt'       **
    pop si                      ; and old text pointer  *

.ip5:
    pop ax                      ; purge junk in stack (si in case of error)
    call ignblnk
    cmp al, ','                 ; Is next char ',' ?
    jne finish
    inc si                      ; Eat the ',' character
    jmp input                   ; Yes, more items.
    
.ip8:
    push si                     ; save fot prtstg
    call tstv                   ; Must be variable now
    jc qwhat                    ; "What?" It is not?
    mov di, bx                  ; destination variable
    mov bx, si                  ; end mark for prtchs
    pop si
    call prtchs                 ; print those as prompt
    jmp .ip3                    ; Yes, input variable
    
.ip12:
    pop bp                      ; return address
    push si                     ; save text pointer     *
    mov ax, [currnt]
    push ax                     ; also save 'currnt'     **
    mov word [currnt], 0xffff   ; use -1 as flag (for 'error')
    mov [stkinp], sp            ; save sp too
    push di                     ; destination variable
    mov al, ' '                 ; print a space
    push bp
    jmp getln                   ; and get a line
    ; jmp above, no need to ret here.


; 'let' is followed by a list of items separated by commas. Each item
; consists of a variable, an equal sign, and an expr. TBI evaluates
; the expr. and set the variable to that value. TBI will also handle
; 'let' command without the word 'let'. This is done by 'deflt'.
deflt:
    mov al, [si]                ; *** deflt ***
    cmp al, 0dh                 ; Empty line is OK
    je finish
let:                            ; Else it is "let"
    call setval                 ; Set value to var
    call ignblnk
    cmp al, ','                 ; item by item
    jne finish                  ; until finish
    inc si
    jmp let
    
    
;=========================================================================
; expr: Expression parser
;
; 'exp' evaluates arithmetical or logical expressions.
; <exp>::=<expr1>
;         <expr1><rel.op><expr1>
; where <rel.op> is one of the operators in table6 and the result of these 
; operations is 1 if true and 0 if false.
; <expr1>::=(+ or -)<expr2>(+ or -<expr2>)(...)
; where () are optional and (...) are optional repeats.
; <expr2>::=<expr3>(<* or /><expr3>)(...)
; <expr3>::=<variable>
;           <function>
;           <digit>
;           (<exp>)
; <exp> is recursive so that variable '@' can have an <exp> as index, 
; functions can have an <exp> as argument, and
; <expr3> can be <exp> in parenthesis.

exp:
    call expr1                  ; *** expr1 ***
    push bx                     ; save <expr1> value
    mov bx, table6              ; Look up rel. op.
    jmp exec                    ; Go do it
expr_ge:                        ; Rel.op. ">="
    call xpr8                   ; 
    pop ax                      ; restore 1st <expr1> in ax
    cmp ax, dx                  ; Make the comparison here
    jge xpr_true                ; True
    ret                         ; Otherwise return with the default False
expr_ne:                        ; Rel.op. "#"
    call xpr8
    pop ax                      ; restore 1st <expr1> in ax
    cmp ax, dx                  ; Make the comparison here
    jne xpr_true
    ret
expr_g:                         ; Rel.op. ">"
    call xpr8
    pop ax                      ; restore 1st <expr1> in ax
    cmp ax, dx                  ; Make the comparison here
    jg xpr_true
    ret
expr_eq:                        ; Rel.op. "="
    call xpr8
    pop ax                      ; restore 1st <expr1> in ax
    cmp ax, dx                  ; Make the comparison here
    je xpr_true
    ret
expr_le:                        ; Rel.op. "<="
    call xpr8
    pop ax                      ; restore 1st <expr1> in ax
    cmp ax, dx                  ; Make the comparison here
    jle xpr_true
    ret
expr_lt:                        ; Rel.op. "<"
    call xpr8
    pop ax                      ; restore 1st <expr1> in ax
    cmp ax, dx                  ; Make the comparison here
    jl xpr_true
    ret
xpr7:
    pop bx                      ; not Rel.op.
    ret                         ; Return value=<expr1>
xpr8:                           ; Subroutine for all Rel.ops.
    call expr1                  ; Get 2nd <expr1>
    mov dx, bx                  ; use dx for the test
    xor bx, bx                  ; Prepare a 'false' value by default
    ret
xpr_true:
    mov bl, 1                   ; Return 'true' value
    ret

expr1:
    call ignblnk
    cmp al, '-'                 ; Negative sign ?
    jne .xp11
    xor bx, bx                  ; Yes, fake "0-"
    jmp .xp16                   ; Treat like substract
.xp11:
    cmp al, '+'                 ; Positive sign ? Ignore
    jne .xp12
    inc si                      ; (but eat it still)
.xp12:
    call expr2                  ; 1st <expr2>
.xp13:
    call ignblnk
    cmp al, '+'                 ; Add?
    jne .xp15
    push bx                     ; Yes, save value
    inc si                      ; and eat the + character
    call expr2                  ; get 2nd <expr2>
    pop ax
    add bx, ax
    jo qhow                     ; Dow we have an overflow ?
    jmp .xp13                   ; Look for more terms
.xp15:
    cmp al, '-'                 ; subtract ?
    jne .xp17
.xp16:
    push bx                     ; yes, save 1st <expr2>
    inc si                      ; Eat the - character
    call expr2                  ; Get the second <expr2>
    pop ax                      ; Restore 1st term
    xchg ax, bx                 ; exchange 1st and 2nd terms
    sub bx, ax                  ; subtract the second from the first
    jo qhow                     ; Do we have an overflow ?
    jmp .xp13                   ; Look for more terms

.xp17:
    ret


expr2:
    call expr3                  ; Get 1st <expr3>
.xp21:
    call ignblnk
    cmp al, '*'                 ; Multiply ?
    jne .xp24
    push bx                     ; Yes, save 1st
    inc si                      ; (eat the * character)
    call expr3                  ; and get second <expr3>
    pop ax                      ; and 1st in ax
    imul bx                     ; dx:ax = ax*bx
    jo qhow                     ; signed result does not fit ax
    mov bx, ax
    jmp .xp21                   ; Look for more terms
.xp24:
    cmp al, '/'                 ; Divide ?
    jne .xp25
    push bx                     ; Yes, save 1st
    inc si                      ; (eat the / character)
    call expr3                  ; and get 2nd <expr3> in bx
    pop ax                      ; get 1st in ax
    or bx, bx                   ; Divide by 0 ?
    jz qhow                     ; say "How?"
    cwd                         ; convert word in ax into dword in dx:ax
    idiv bx                     ; divide
    mov bx, ax                  ; return result in bx
    jmp .xp21                   ; Look for more terms
.xp25:
    ret                         ; Done.

    
expr3:                          
    mov bx, table3              ; Find function in table3
    jmp exec
notf:                           ; No, not a function
    call tstv                   ; Is it a variable ?
    jc .xp32                    ; Nor a variable
    mov bx, [bx]                ; Load variable value in bx
    ret
.xp32:
    call tstnum                 ; or is it a number ?
    or cl, cl
    jz parn                     ; No digit, must be "(expr)"
    mov bx, ax                  ; OK, digit in bx
    ret
    

parn:                           ; "(expr)" at si->
    lodsb                       ; eat the character
    cmp al, '('                 ; is it '(' ?
    jne qwhat                   ; no, say "what?"
    call exp                    ; evaluate expression
    lodsb                       ; eat the character
    cmp al, ')'                 ; is it ')' ?
    jne qwhat                   ; no, say "what?"
    ret

;=========================================================================
; Functions.

rnd:                            ; Simple pseudo-random generator
    call parn
    cmp bx, 0                   ; expr must be strictly positive
    jng qhow                    ; or it is an error.

    ; RND implementations suggested by Trixter on 
    ; http://www.vcfed.org/forum/showthread.php?40098-Palo-Alto-Tiny-Basic-Download&p=302299#post302299
%ifdef GOOD_RND
    ; *** "This claims 2^31-1 repeatability but I've never verified it."
    push bx
    mov ax, [ranseed+2]
    mov dx, [ranseed]
    mov bx, ax
    mov cx, dx
    shl cx, 1                   ; )
    shl cx, 1                   ; > original was shl cx, 3
    shl cx, 1                   ;_)________________________
    shr bh, 1                   ; )
    shr bh, 1                   ; \ original was shr bh, 4
    shr bh, 1                   ; /
    shr bh, 1                   ; )
    or cl, bh
    xor dx, cx
    not dx
    shl dx, 1
    rcl ax, 1
    shr dx, 1
    mov [ranseed], ax
    mov [ranseed+2], dx
    pop bx
    jmp .rnd_modulo
%else
    ; *** "Another way, if you don't care too much about periodicity and 
    ;      just want small fast results:"
    mov ax, [ranseed]           ;
    add ax, 9248h               ; 1001001001001000b (visual rep)
    ror ax, 1
    ror ax, 1
    ror ax, 1
    mov [ranseed], ax           ; Quick and dirty, it should be improved.
%endif

.rnd_modulo:
    xor dx, dx
    div bx                      ; we tested the arg. to be positive
    mov bx, dx                  ; remainder of the division by the argument
    inc bx                      ; value must be in range [1..arg] inclusive.
    ret

_abs:
    call parn                   ; abs(expr)
    or bx, bx                   ; Prepare for comparison
    jns .pos                    ; Is it already positive ?
    neg bx                      ; it's signed, then neg it
.pos:
    ret


size:
    mov bx, txtend              ; Get the number of free bytes between txtunf
    sub bx, [txtunf]            ; and txtend
    ret

;=========================================================================
; 'divide', 'subde', 'chksgn', 'chgsgn' and 'ckhlde' are unneeded.


;=========================================================================
; 'setval' expects a variable, followed by an equal sign and then an
; expr. It evaluates the expr. and set the variable to that value.
setval:
    call tstv
    jc qwhat                    ; "What?" no variable
    push bx                     ; save address of var
    call ignblnk
    cmp al, '='                 ; pass "=" sign
    jne qwhat
    inc si                      ; Eat the character
    call exp                    ; Evaluate expression
    pop di
    mov [di], bx                ; Save value
    ret


; 'fin' checks the end of a command. If it ended with ";", execution
; continues. If it ended with a CR, it finds the next line and
; continue from there.
finish:
    call fin                    ; Check end of command
    jmp qwhat                   ; Print "What?" if wrong
fin:
    call ignblnk
    cmp al, ';'
    jne .fi1
    pop ax                      ; dummy pop, purge ret. address
    inc si                      ; Eat the character
    jmp runsml                  ; continue same line
.fi1:
    cmp al, 0dh                 ; not ";", is it CR?
    jne .fi2
    pop ax                      ; dummy pop, purge ret. address
    inc si                      ; eat the character
    jmp runnxl                  ; Run next line
.fi2:
    ret                         ; Else, return to caller


; 'ignblnk' ignore blanks in text -> si by advancing si to the first non-blank
; character. 
; That character is also returned in al
ignblnk:
    lodsb                       ; Load character
    cmp al, ' '                 ; is it a space ?
    je ignblnk                  ; Then continue
    dec si                      ; Otherwise, move si back to that character
    ret                         ; And return with the char in al


; 'endchk' checks if a command is ended with CR. This is required on certain 
; commands (GOTO, RETURN, STOP, etc.)
endchk:                         ; *** end check ***
    call ignblnk
    cmp al, 0dh                 ; End with CR ?
    jne qwhat                   ; if no, say "what?"
    ret                         ; else, simply return
    

; Related to 'error' are the following: 'qwhat' saves text pointer in
; stack and get message "What?". 'awhat' just get message "What?" and
; jump to 'error'. 'qsorry' and 'asorry' do same kind of thing.
; 'qhow' and 'ahow' also do this.
qwhat:
    push si
awhat:
    mov si, what
    ; and continue to error here below
    
; 'error' prints the string pointed by dx (and ends with CR). It then 
; prints the line pointed by 'currnt' with a "?" inserted at where the
; old text pointer (should be on top of the stack) points to. 
; Execution of TB is stopped and TBI is restarted. However, if 
; 'currnt' -> 0 (indicating direct command), the direct command is not 
; printed. And if 'currnt' -> negative # (indicating 'input' 
; command), the input line is not printed and execution is not 
; terminated, but continued at inperr.
error:
    call crlf
    xor ah, ah
    call prtstg                 ; Print error message pointed by dx
    mov si, [currnt]            ; get current line #
    or si, si                   ; check the value
    jz rstart                   ; if zero, just restart
    cmp si, 0xffff              ; if -1
    je inperr                   ; redo input
    call prtln                  ; Else print the line
    pop bx                      ; Restore pointer to where error happened
    call prtchs
    mov al, '?'                 ; print a "?"
    call outch
    call prtstg                 ; print the rest of the line
    jmp rstart                  ; then restart
    

qsorry:
    push si
asorry:
    mov si, sorry
    jmp error
    

;=========================================================================
; 'fndln' finds a line with a given line # (in ax ) in the text save area.
; si is used as the text pointer. 
; If the line is found, si will point to the beginning of that line (i.e. the 
; low byte of the line #), and flags are NC and Z.
; If that line is not there and a line with a higher line # is found, si points
; to there, and flags are NC and NZ.
; If we reached the end of the text save area and cannot find the line, flags 
; are C and NZ.
; 'fndln' will initialize si to the beginning of the text save area to start 
; the search. Some other entries if this routine will not initialize si and do 
; the search.
; 'fndlnp' will start with si and search for the line #.
; 'fndnxt' wil bump si by 2, find a CR, and then start search.
; 'fndskp' will use si to find a CR and then start search.
; Only si and flags are modified.
fndln:
    or ax, ax                   ; Check sign of ax
    js qhow                     ; It cannot be negative
    mov si, txtbgn              ; init text pointer
    
fndlnp:                         ; *** fndlnp ***
    push bx                     ; save bx
    mov bx, [txtunf]            ; check if we passed end
    dec bx
    cmp bx, si                  ; when si = [txtunf], this will overflow
    pop bx                      ; restore bx
    jc .flnret                  ; C, NZ: passed end
    
    cmp [si], ax                ; Is it the line # we are looking for?
    jb fndnxt                   ; No, not there yet
.flnret:                        ; else we either found it or it is not there
    ret                         ; NC,Z: found;  NC,NZ: not found

fndnxt:                         ; find next line
    inc si                      ; 
fl1:
    inc si                      ; Just passed byte 1 and 2
    
fndskp:                         ; Try to find CR
    cmp byte [si], 0dh          ; is it CR ?
    jne fl1                     ; keep looking
    inc si                      ; found CR, skip over
    jmp fndlnp                  ; check if end of text
   
   
; 'tstv' is used to check for a variable; either single-letter variable or 
; the @() array variable. 
; 
; The address of the variable is returned in bx.
tstv:                           ; Test for a variable
    call ignblnk
    cmp al, '@'                 ; is it the array variable ?
    ;jc .end_tstv                ; C: not a variable
    jne .test_charv             ; not '@' array
    
    inc si                      ; It is the @ array
    call parn                   ; @ should be followed by (expr) as its index
    shl bx, 1                   ; word storage
    jc qhow                     ; Is index too big ?
    
    push dx                     ; Will it fit ?
    mov dx, bx
    call size                   ; Find size of free
    cmp dx, bx                  ; And check that
    jge qsorry                  ; If not, say sorry

    mov bx, txtend              ; If it fits, get address
    sub bx, dx                  ; of @(expr) and put it in bx
    pop dx                      ; restore dx
    clc
    ret                         
    
.test_charv:
    or al, 20h                  ; Lowercase al
    sub al, 'a'                 ; 
    cmp al, 26                  ; Not @, is it 'a' to 'z' ?
    ja .no_tstv                 ; If not, return with CF
    xor ah, ah                  
    shl ax, 1                   ; (word storage)
    mov bx, varbgn              
    add bx, ax                  ; bx = varbgn + ax*2
    inc si                      ; Advance pointer (eat variable char)
    clc
    ret
    
.no_tstv:
    stc                         ; Not a variable, set CF
    ret


;=========================================================================
; 'tstch' is not used in this implementation. Instead, the call to 'ignblnk'
; pre-loads the 1st non-blank character in al, ready to be compared.

    
; 'tstnum' is used to check whether the text (pointed by si) is a number.
; If a number is found, ax contains the number's value and cl will contain the
; number of digits. If not, ax and cl are set to zero.
; si is advanced to the non-digit character.
; Trashed: bx
tstnum:
    call ignblnk                ; Skip any spaces. Char is already in al
    xor ax, ax
    xor bx, bx                  ; Initialize. 
    xor cl, cl                  ; 

.tstnumch:    
    lodsb                       ; char in al
    cmp al, '0'                 ; Is it '0' or above ?
    jb .not_digit               ; no, then it can't be a digit.
    cmp al, '9'                 ; Is it '9' or below ?
    ja .not_digit               ; no, then it can't be a digit.
    
    ; We have a digit.
    test bh, 0f0h               ; If any of the four high bits are set in bh,
    jnz qhow                    ; there will be no room for the next digit.
    
    inc cl                      ; count one more digit
    
    ; a 'mul' is more than 100 cycles!
    push dx                     ; save dx
    shl bx, 1                   ;   bx = old * 2
    mov dx, bx                  ;   dx also = old * 2
    shl bx, 1                   ;   bx = old * 4
    shl bx, 1                   ;   bx = old * 8
    add bx, dx                  ;   bx = old * 8 + old * 2 = old * 10
    pop dx                      ; restore dx
    
    and al, 0fh                 ; convert from ascii '0' to numeric value
    add bx, ax                  ; add that to bx
    js qhow                     ; If we wrapped to signed, that's an error
    jmp .tstnumch               ; Do next char

.not_digit:
    dec si                      ; move si back to that char that was not a digit
    mov ax, bx                  ; result in ax. cl should be ok.
    ret

qhow:                           ; *** Error : How? ***
    push si
ahow:
    mov si, how
    jmp error


;=========================================================================
; 'mvup' and 'mvdown', as declared in the original tbasic, are not really worth
; reimplementing for a x86 CPU that has built-in string operation like 'movsb',
; and even more so with the 'rep' prefix.

; '_popa' restores the 'for' loop variable save area from the stack
_popa:
    pop bp                      ; bp=return address
    pop ax                      ; restore lopvar, but
    mov [lopvar], ax
    or ax, ax                   ; =0 means no more
    jz .pp1
    pop ax
    mov [lopinc], ax
    pop ax
    mov [loplmt], ax
    pop ax
    mov [lopln], ax
    pop ax
    mov [loppt], ax
.pp1:
    push bp
    ret


; '_pusha' stacks the 'for' loop variable area into the stack
_pusha:
    pop bp                      ; bp=return address
    cmp sp, stklmt              ; Is stack near the top ?
    jl qsorry                   ; Yes, sorry for that.
    mov ax, [lopvar]            ; Else save loop vars.
    or ax, ax                   ; But if lopvar is 0
    jz .pu1                     ; That will be all
    mov ax, [loppt]
    push ax
    mov ax, [lopln]
    push ax
    mov ax, [loplmt]
    push ax
    mov ax, [lopinc]
    push ax
    mov ax, [lopvar]
.pu1:
    push ax
    push bp                     ; bp = return address
    ret


;=========================================================================
; 'prtstg' prints a string pointed to by si. It stops printing and returns
; to caller when either a CR is printed or when the next byte is equal to ah.
prtstg:
.psloop:
    lodsb                       ; Get a char
    cmp al, ah                  ; same as ah ?
    je .psend                   ; yes, we're done
    call outch                  ; else print it
    cmp al, 0dh                 ; was it a CR ?
    jne prtstg                  ; no, next char
.psend:
    ; si points after stop char or CR.
    ret


; 'prtstg_cs' acts just like prtstg, but with text pointed to by cs:dx, for 
; use with strings declared in ROM.
;prtstg_cs:
;    push ds                     ; save ds
;    push cs
;    pop ds                      ; ds now points to code
;    xchg si, dx
;    call prtstg
;    xchg dx, si
;    pop ds                      ; restore ds
;    ret


; 'qtstg' looks for caret (^), singe quote or double quote at si-> (al set)
; If none of these, return to caller. If caret, output a control
; character. If single or double quote, print the string in the quote
; and demands matching unquote. On return, if there was a quoted string 
; or a control character to print, CF is cleared. Otherwise, CF is set.
qtstg:
    cmp al, 022h                ; Is it a " ?
    je .qt1
    cmp al, 027h                ; Is it a ' ?
    jne .qt4
.qt1:
    mov ah, al                  ; Yes, it is a " or a '
    inc si                      ; Eat the quote character
    call prtstg                 ; print until another
.qt2:
    cmp al, 0dh                 ; Was last one a CR?
    jne .qt3
    pop ax                      ; Dummy pop of return address
    jmp runnxl                  ; Was CR, run next line
.qt3:
    clc                         ; Clear CF
    ret                         ; Return
.qt4:
    cmp al, '^'                 ; Is it a caret ?
    jne .qt5
    inc si
    lodsb                       ; Yes, convert next character
    xor al, 0x40                ; to Control-char
    call outch
    mov al, [si]                ; Load next char in al
    jmp .qt2                    ; Just in case it is a CR
    
.qt5:                           ; None of the above
    stc                         ; Set CF
    ret
    

; 'prtchs' prints a string pointed to by si, until si is equal to bx.
; At exit, si points to the next, not printed yet, character.
prtchs:
    cmp si, bx
    jge .pc1
    lodsb
    call outch
    jmp prtchs
.pc1:
    ret
    

; 'prtnum' prints the number in ax. Leading blanks are added if needed to pad the
; number of spaces to the number in cl. However, if the number of digits is 
; larger than cl, all digits are printed anyway. Negative sign is also printed 
; and counted in, positive sign is not.
prtnum:
    push ax                     ; save registers
    push bx
    push cx
    push dx
    mov bx, 10                  ; decimal. Cannot appear as a digit.
    xor ch, ch                  ; By default, no sign
    or ax, ax                   ; check sign
    jns .unsigned               ; our number is positive
    mov ch, '-'                 ; ch contains sign
    dec cl                      ; '-' sign takes place
    neg ax                      ; make ax positive,
.unsigned:
    push bx                     ; Save as a flag
.pn5:
    xor dx, dx                  ; set dx to 0 for division
    div bx                      ; result in ax, remainder in dx
    or ax, ax                   ; result O ?
    jz .pn6                     ; Yes, we got all
    push dx                     ; save remainder
    dec cl                      ; dec space count
    jmp .pn5                    ; Divide by 10 again
.pn6:                           ; We got all digits in the stack
    mov al, ' '                 ; Leading blank
.pn7:
    dec cl                      ; look at space count
    jle .pn8                    ; no (more) leading blanks
    call outch
    jmp .pn7                    ; more ?
.pn8:
    mov al, ch                  ; print sign
    or al, al
    jz .nosign                  ; maybe - or null
    call outch
.nosign:
    mov ax, dx                  ; Last remainder was in dx
.digit:
    cmp al, bl                  ; bl=10, it is our flag for no more
    je .end
    add al, '0'                 ; convert to ascii
    call outch                  ; and print the digit
    pop ax                      ; get next digit
    jmp .digit                  ; and go back for more
.end:
    pop dx                      ; Restore registers
    pop cx
    pop bx
    pop ax
    ret


; 'prtln' prints the line number pointed to by si followed by a space.
; si is also advanced by two
prtln:
    push cx                     ; save cx
    mov ax, [si]                ; load the number
    mov cl, 4                   ; Line number by default on 4 chars
    call prtnum                 ; print the line #
    mov al, ' '                 ; print a space
    call outch
    inc si                      ; Advance si past the word value that has been
    inc si                      ; printed.
    pop cx                      ; restore cx
    ret
    
; Direct commands:
table1:
    db  'list', 0
    dw  list
    db  'new', 0
    dw  new
    db  'run', 0
    dw  run


; Direct/Statements:
table2:
    db  'next', 0
    dw  next
    db  'let', 0
    dw  let
    db  'if', 0
    dw  iff
    db  'goto', 0
    dw  goto
    db  'gosub', 0
    dw  gosub
    db  'return', 0
    dw  return
    db  'rem', 0
    dw  rem
    db  'for', 0
    dw  for
    db  'input', 0
    dw  input
    db  'print', 0
    dw  print
    db  'stop', 0
    dw  stop
    db  0
    dw  deflt
    
; Functions
table3:
    db 'rnd', 0
    dw rnd
    db 'abs', 0
    dw _abs
    db 'size', 0
    dw size
    db 0
    dw notf
    
; "For" command
table4:
    db  'to', 0
    dw  for_to
    db  0
    dw  qwhat
    
table5:
    db  'step', 0
    dw  for_step
    db  0
    dw  fr3

; Relation operators
table6:
    db '>=', 0
    dw expr_ge
    db '#', 0
    dw expr_ne
    db '>', 0
    dw expr_g
    db '=', 0
    dw expr_eq
    db '<=', 0
    dw expr_le
    db '<', 0
    dw expr_lt
    db 0
    dw xpr7


;=========================================================================
; 
; Input output routines
;
; User must verify and/or modify these routines.
 

; 'crlf' will output a CR followed by its LF.
; al is changed (set to 0dh, the CR character)
crlf:
    mov al, 0dh                 ; CR in al
    ; then continues with outch


; 'outch' will output the character in al. If the character is CR, it 
; will also output a LF. Only flags may change at return.
outch:
    push ax
    push dx
    
    mov ah, 06h                 ; Write Character to standard output
    mov dl, al
    int 21h                     ; Send the character
    
    cmp al, 0dh                 ; is it a CR ?
    jne .done
    
    mov ah, 02h                 ; send the LF along.
    mov dl, 0ah
    int 21h
.done:
    pop dx
    pop ax
    ret

; 'chkio' checks to see if there is any input. If no input, it returns zero in 
; al with ZF set. If there is input, it further checks whether input is
; control-C. If not control-C, it returns the character in al and ZF is
; cleared. If input is control-C, 'chkio' jumps to 'init' and will not 
; return. Only ax and flags may change at return.
chkio:
    mov ah, 1                   ; Query keyboard status
    int 16h
    jz .end                     ; Nothing to read, return with ZF still set
    
    xor ah, ah
    int 16h                     ; Key is ready, get it
    
    cmp al, 18h                 ; Is it Control-X ?
    je .exit

    cmp al, 03h                 ; is it Control-C ?
    jne .end                    ; No, then return. ZF is conveniently no set.
    jmp rstart                  ; yes, restart TBI

.end:
    ret

.exit:
    mov ax, 4c00h               ; Back to DOS
    int 21h
    
; 'getln' reads a input line into 'buffer'. It first prompts the character 
; in al (given by the caller), then it fills the buffer and echos. Backspace 
; is used to delete the last character (if there is one). CR signals the end 
; of the line, and causes 'getln' to return. 
; When 'buffer' is full, 'getln' will accept backspace or CR only and will
; ignore (and will not echo) other characters. After the input line is stored
; in the buffer, di points beyond the last CR.
; ax and flags are also changed at return.
getln:
    call outch                  ; write the prompt from al
    mov di, buffer              ; getln will store the line into the buffer
.glinloop:
    hlt                         ; Wait until an interrupt arrives
    call chkio                  ; get a character
    jz .glinloop                ; Wait for input
    ; PATB seems to ignore LF here
    
    cmp al, 08                  ; Is it backspace ?
    jne .gltestcr               ; No, check for other special cases
    cmp di, buffer
    jna .glinloop               ; Buffer is already empty
    call outch                  ; go back on screen
    mov al, ' '
    call outch                  ; print space over
    mov al, 08
    call outch                  ; go back again
    dec di                      ; pointer goes back too
    jmp .glinloop               ; Go get next char

.gltestcr:
    cmp al, 0dh                 ; is it CR ?
    je .gleol                   ; Yes, end of line
    cmp di, bufend              ; Else, do we have room ?
    je .glinloop                ; No, wait for CR or BS
    
    call outch                  ; We have room, echo the character
    stosb                       ; and store it in the buffer
    jmp .glinloop               ; Go get next one
    
.gleol:
    call outch                  ; Echo the char (CR)
    stosb                       ; and store it in the buffer.
    ret

section .data
;========================================================================
; Initialized data
msg             db      'PATB86 v3.0, Amand Tihon, 2018', 0dh
ok              db      'Ok', 0dh
what            db      'What?', 0dh
how             db      'How?', 0dh
sorry           db      'Sorry', 0dh


section .bss
;========================================================================
; Uninitialized data
; Define space for variables, text space and buffer
currnt          resw 1          ; word - points tu current line
stkgos          resw 1          ; word - saves sp in 'gosub'
varnxt          resw 1          ; word - temp storage
stkinp          resw 1          ; word - saves sp in 'input'
lopvar          resw 1          ; word - 'for' loop save area
lopinc          resw 1          ; word - increment
loplmt          resw 1          ; word - limit
lopln           resw 1          ; word - line number
loppt           resw 1          ; word - text pointer
ranseed         resd 1          ; dword - random number seed
outcar          resb 1          ; byte - output char storage

txtbgn          resb 32000      ; byte[~32k] - text area
txtend:                         ; text area end address

buffer          resb 128        ; byte[128]
bufend          resb 4          ; Only a safeguard
                
varbgn          resw 26         ; 26 variables. 

txtunf          resw 1          ; word - unfilled text area

;========================================================================
; Stack definition
stkbgn          resb    10h     ; Soft top stack limit
stklmt          resb    0400h   ; That's not a lot, but that can be expanded
stack:                          ; Stack starts here

