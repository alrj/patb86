# PATB 86

Li-Chen Wang's Tiny Basic ported to x86.


This Palo-Alto Tiny Basic was initially written for Intel 8080 processors, and
was ported to Z80 shortly after its publication in Dr. Dobbs Journal.

More than 40 years later, here it is, ported to Intel x86. It is kept 100% 
compatible with Intel 8088 though.

The project currently compiles as a DOS .COM executable file.

Learn more about [Tiny Basic on Wikipedia](https://en.wikipedia.org/wiki/Tiny_BASIC).

The work done by [mmruzek on the vcfed.org forums](http://www.vcfed.org/forum/showthread.php?40098-Palo-Alto-Tiny-Basic-Download) 
was an incredibly useful reference to me during the entire porting adventure,
as I am not really familiar with Z80/8080 assembler.

I hope it can be useful to anyone.
