    1 REM Mandelbrot set for PATB version 3
    2 REM Copyleft Amand Tihon 2018, all wrongs reversed.
   10 for r=-72 to 72 step 6
   20 for c=-120 to 80 step 3
   30 x=0, y=0, i=0, d=c-20
   40 t=(x*x-y*y)/60+d, y=(2*x*y)/60+r, x=t, i=i+1
   50 if i<30 if (x/60*x+y/60*y) <= (240) goto 40
   60 v=5
   70 if i<20 v=i/4
   80 gosub 200+v
   90 next c
  100 print
  110 next r
  120 stop
  200 print " ",; r.
  201 print ".",; r.
  202 print ";",; r.
  203 print "%",; r.
  204 print "$",; r.
  205 print "#",; r.
