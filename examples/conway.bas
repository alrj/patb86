    1 REM Conway's game of life for PATB version 3
    2 REM Copyleft Amand Tihon 2018, all wrongs reversed
   10 REM Random pattern:
   20 for i=0 to 264; @(i)=(rnd(2)=1); next i
   30 REM Glider test:
   40 REM for i=0 to 264; @(i)=0; next i
   50 REM @(23)=1, @(46)=1, @(68)=1, @(25)=1, @(47)=1
   90 print ^[, "[2J",
  100 print ^[, "[1;1H", "+--------------------+"
  110 for y=22 to 220 step 22; @(y)=@(y+20), @(y+21)=@(y+1); next y
  120 for x=1 to 20; @(x)=@(220+x), @(242+x)=@(22+x); next x
  130 @(0)=@(240), @(263)=@(23), @(21)=@(220), @(242)=@(42)
  140 for y=22 to 220 step 22; print "|",; for x=1 to 20
  150 c=y+x, d=c+300; if @(c) print "#",; goto 170
  160 print " ",
  170 n = @(c-23)+@(c-1)+@(c+21)+@(c-22)+@(c+22)+@(c-21)+@(c+1)+@(c+23)
  190 if n = 3 @(d) = 1; goto 220
  200 if @(c)*(n=2) @(d) = 1; goto 220
  210 @(d) = 0
  220 next x; print "|"; next y
  230 print "+--------------------+"
  240 for i=0 to 263; @(i)=@(i+300); next i
  250 goto 100
