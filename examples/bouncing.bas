  1 REM Bouncing ball for PATB version 3
  2 REM Copyleft Amand Tihon 2018, all wrongs reversed.
 10 print ^[, "[2J",
 20 print "+------------------------------------------------------------+"
 30 for i=1 to 22
 32 print "|                                                            |"
 34 next i
 40 print "+------------------------------------------------------------+"
 50 x=rnd(58)+1, y=rnd(20)+1, r=rnd(2), u=(r=1)-(r=2), v=-1, a=x, b=y
 60 gosub 1000; print "o", ^H,; x=x+u, y=y+v
 70 if x < 2 x=3, u=-u
 80 if x > 61 x=60, u=-u
 90 if y < 2 y=3, v=-v
100 if y > 23 y=22, v=-v
110 a=x, b=y; print " ",
120 goto 60
1000 print ^[, "[", #1, b, ";", a, "H",; return
